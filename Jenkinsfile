def summary = ''

pipeline {
    
    agent any
    
    options {
		timestamps()
		}
        
    tools{
		    maven 'Default'
			jdk 'AdoptOpenJDK 8'
		}

    stages {
        
        stage('Scan') {
			steps {
				echo 'Running Sonarqube Scan...'
				withSonarQubeEnv('SonarQube') {
					sh 'mvn clean sonar:sonar'
				}
				timeout(time: 5, unit: 'MINUTES') {
						waitForQualityGate abortPipeline: true
				}       
			}
		}

        stage('Build') { 
            steps {
                echo 'Packaging ...'
                sh 'mvn -f pom.xml clean package -DskipTests=true'
          }
        }
        
        stage('Test') { 
            steps {
                echo 'Testing ...'
                configFileProvider([configFile(fileId: 'mule_enterprise_repo_maven_settings', variable: 'MAVEN_SETTINGS_XML')]) {
                sh 'mvn -f pom.xml -s $MAVEN_SETTINGS_XML test'
				
                //publish coverage report
                publishHTML target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'target/site/munit/coverage',
                    reportFiles: 'summary.html',
                    reportName: 'MUnit Coverage Report'
                    ]
                }
                
                //publish test results
                script {
                    summary = junit testResults: 'target/surefire-reports/TEST-*.xml'
                }
                slackSend channel: '#mule-ci-cd',
                    color: '#000dbd',
                    message: "\n *MUnit Test Summary* - Total: ${summary.totalCount}, Failures: ${summary.failCount}, Skipped: ${summary.skipCount}, Passed: ${summary.passCount}\n*MUnit Test Details* - ${env.BUILD_URL}testReport/\n*MUnit Coverage Report* - ${env.BUILD_URL}MUnit_20Coverage_20Report/"                
            }
        }
        
        stage('Archive') {
          steps {
              echo 'Uploading ...'
              configFileProvider([configFile(fileId: 'mule_enterprise_repo_maven_settings', variable: 'MAVEN_SETTINGS_XML')]) {
              sh 'mvn -f pom.xml deploy -DskipTests=true -s $MAVEN_SETTINGS_XML'
            }
          }
        }
	}
	
    post {
        always {
            echo 'Cleaning workspace...'
            deleteDir() /* clean up our workspace */
        }
        success {
            echo 'I succeeded :)'
            slackSend channel: '#mule-ci-cd',
                color: '#00b000',
                    message: "The pipeline *${currentBuild.fullDisplayName}* ${env.BUILD_URL} has completed successfully."
        }
        unstable {
            echo 'I am unstable :/'
            slackSend channel: '#mule-ci-cd',
                color: '#f0ec00',
                    message: "The pipeline *${currentBuild.fullDisplayName}* ${env.BUILD_URL} is unstable."
        }
        failure {
            echo 'I failed :('
            slackSend channel: '#mule-ci-cd',
                color: '#e80000',
                    message: "The pipeline *${currentBuild.fullDisplayName}* ${env.BUILD_URL} has failed."
        }
    }
}